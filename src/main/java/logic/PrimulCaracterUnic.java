package main.java.logic;

import java.util.Arrays;

public class PrimulCaracterUnic {
    static final int NO_OF_CHARS = 256;
    static char count[] = new char[NO_OF_CHARS];

    /* calculate count of characters
       in the passed string */
    public static void getNumaraCaractereArray(String sir) {
        for (int i = 0; i < sir.length(); i++)
            count[sir.charAt(i)]++;
//        System.out.println(Arrays.toString(count));
    }

    /* The method returns index of first non-repeating
       character in a string. If all characters are repeating
       then returns -1 */
    public static int primulCaracterUnic(String sir) {
        getNumaraCaractereArray(sir);
        int index = -1, i;

        for (i = 0; i < sir.length(); i++) {
            if (count[sir.charAt(i)] == 1) {
                index = i;
                break;
            }
        }

        return index;
    }
}
