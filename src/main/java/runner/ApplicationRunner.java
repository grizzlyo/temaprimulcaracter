package main.java.runner;

import main.java.logic.PrimulCaracterUnic;

import java.util.Scanner;


public class ApplicationRunner {
    public static void main(String[] args) {
        PrimulCaracterUnic primulCaracterUnic = new PrimulCaracterUnic();
        Scanner s = new Scanner(System.in);
        System.out.println("Introduceti sirul: ");
        String sir = s.nextLine();

        int index = primulCaracterUnic.primulCaracterUnic(sir);


        System.out.println(index == -1 ? "Nu exista caractere unice in sir " +
                "sau sirul este gol" : "Primul caracter unic din sir este: " + sir.charAt(index));

    }
}


