package test.java.logic;

import main.java.logic.PrimulCaracterUnic;
import org.junit.Assert;
import org.junit.Test;

public class PrimulCaracterUnicTest {
    @Test
    public void primulCaracterUnicTest(){
        System.out.println("Metoda primulCaracterUnicTest a fost apelata!");
        PrimulCaracterUnic primulCaracterUnic = new PrimulCaracterUnic();
        int primulCharUnic = primulCaracterUnic.primulCaracterUnic("aaee");
        Assert.assertEquals(-1, primulCharUnic);
    }
}
